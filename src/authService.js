const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;
  const createdDate = new Date().toISOString();

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
    createdDate
  });

  user.save()
    .then(() => res.status(200).send({ "message": "Success" }))
    .catch((e) => {
      res.status(400).send({ "message": "Register error" });
    });
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, userId: user._id };
    const jwtToken = jwt.sign(payload, 'jwt_token');
    return res.status(200).json({ message: 'Success', jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
