const jwt = require('jsonwebtoken');
const { Note } = require('./models/Notes');

function createNote(req, res, next) {
  try {
    const { text } = req.body;
    const createdDate = new Date().toISOString();
    const userId = req.user.userId; 
    const note = new Note({
      text,
      userId,
      createdDate
    });
  
    note.save().then(() => {
      res.status(200).send({ "message": "Success" });
    });
  } catch (e) {
    res.status(400).send({ "message": "Creating error" });
  }
  
}

const getMyNotes = (req, res, next) => {
  try {
    return Note.find({ userId: req.user.userId }, '-__v').then((result) => {
      const response = {
        offset: req.query.offset,
        limit: req.query.limit,
        count: result.length,
        notes: result
      }
      res.json(response);
    });
  } catch (e) {
    res.status(400).send({ "message": "Get notes error" });
  } 
} 

function getNote (req, res, next) {
  try {
    return Note.findById(req.params.id)
      .then((response) => {

        const result = {
          note: {
            _id: response._id,
            userId: response.userId,
            completed: response.completed,
            text: response.text,
            createdDate: response.createdDate
          }
        }
        res.json(result);
      });
  } catch (e) {
    res.status(400).send({ "message": "Get note error" });
  } 
}

const deleteNote = (req, res, next) => {
  try {
    Note.findByIdAndDelete({ _id: req.params.id, userId: req.user.userId })
  .then((note) => {
    res.status(200).send({ "message": "Success" });
  });
  } catch (e) {
    res.status(400).send({ "message": "Delete error" });
  }
} 

const updateMyNoteById = (req, res, next) => {
  try {
    const { text } = req.body;
  return Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { text } })
    .then(() => {
      res.status(200).send({ "message": "Success" });
    });
  } catch (e) {
    res.status(400).send({ "message": "Update error" });
  }
};

const markMyNoteCompletedById = async (req, res, next) => {
  try {
    const { text } = req.body;
    const note = await Note.findById({ _id: req.params.id, userId: req.user.userId });
    const { completed } = note;
    note.completed = !completed;

    return note.save().then(() => {
      res.status(200).send({ "message": "Success" });
    });
  } catch (e) {
    res.status(400).send({ "message": "Completed change error" });
  }
};  

module.exports = {
  createNote,
  getNote,
  deleteNote,
  getMyNotes,
  updateMyNoteById,
  markMyNoteCompletedById,
};
