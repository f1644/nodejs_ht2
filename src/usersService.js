const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

function getUserInfo (req, res, next) {
  try {
    return User.findById(req.user.userId)
      .then((result) => {
        const response = {
          user: {
            _id: req.user.userId,
            username: result.username,
            createdDate: result.createdDate
          }
        }
        console.log(result)
        res.json(response);
      });
  } catch (e) {
    res.status(400).send({ "message": "Get user info error" });
  } 
}

const changeUserPassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById(req.user.userId);
  if (user && await bcrypt.compare(String(oldPassword), String(user.password))) {
    return User.findByIdAndUpdate(req.user.userId, { $set: { password:  await bcrypt.hash(newPassword, 10)} })
    .then(() => {
      res.status(200).send({ "message": "Success" });
    });
  }
  return res.status(400).json({ message: 'Wrong password' });
};

const deleteUser = (req, res, next) => {
  try {
    User.findByIdAndDelete(req.user.userId)
  .then(() => {
    res.status(200).send({ "message": "Success" });
  });
  } catch (e) {
    res.status(400).send({ "message": "Delete error" });
  }
};


module.exports = {
  getUserInfo,
  changeUserPassword,
  deleteUser
};
