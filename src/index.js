const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://Kate:Some-password@cluster0.7qdjksr.mongodb.net/notesApp?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter');
const { authRouter } = require('./authRouter');
const { usersRouter } = require('./usersRouter');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));



app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);


const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
