const express = require('express');

const router = express.Router();
const {
  createNote, getNote, deleteNote, getMyNotes, updateMyNoteById,
  markMyNoteCompletedById,
} = require('./notesService.js');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getMyNotes);

router.get('/:id', authMiddleware, getNote);

router.patch('/:id', authMiddleware, markMyNoteCompletedById);

router.put('/:id', authMiddleware, updateMyNoteById);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
