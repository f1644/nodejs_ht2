const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  }
});

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note,
};
