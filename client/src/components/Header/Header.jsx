import './Header.css';
import { useNavigate, Link } from "react-router-dom";


function Header({ token }) {
  let navigate = useNavigate();
  const logout = () => {
    localStorage.removeItem('token');
    navigate("/login", { replace: true });
    window.location.reload();
  }

  return (
    <div className="header">
      <h1 className='header__logo'>Notes</h1>

      {token ? (
        <button className='header__btn' onClick={logout}>Log out</button>
      ) : (
        <div></div>
      )}
      
    </div>
  )
}

export default Header;
