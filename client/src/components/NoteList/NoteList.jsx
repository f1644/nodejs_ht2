import { useState, useEffect } from 'react';
import { useNavigate, Link } from "react-router-dom";
import './NotesList.css';

import { getAllUserNotes } from '../../services';
import NoteItem from '../NoteItem/NoteItem';

function NoteList() {
  const [notes, setNotes] = useState([]);
  const token = localStorage.getItem('token');

  useEffect(() => {
    getAllUserNotes(token)
      .then(((data) => {
        setNotes(data.notes);
      }));
  }, [])

  
  return (
    <div className='notes'>
      {notes.length > 0 ? (
        notes.map((note) => (
          <NoteItem 
            key={note._id}
            id={note._id}
            token= {token}
            text={note.text}
            completed={note.completed}
          />
        ))
      ) : (
        <h2 className='courses__text--notfound'>Notes list is empty</h2>
      )}
      
    </div>
    
  )
}

export default NoteList;
