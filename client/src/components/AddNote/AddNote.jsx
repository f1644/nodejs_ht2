import './AddNotes.css';
import { Link } from "react-router-dom";

function AddNotes() {
  return (
    <div className="add__notes">
      <Link className='link__add' to={`/add`}>Add</Link>
    </div>
  )
}

export default AddNotes;
