import './NoteItem.css';

import { useState } from 'react';

import { changeCompletedNote } from '../../services';

function NoteItem({text, completed, id, token}) {
  const [checked, setChecked] = useState(completed);

  const handleChange = () => {
    setChecked(!checked);
    changeCompletedNote(id, token);
  };

  return (
    <div className='note'>
      <div>{text}</div>
      <input
        type="checkbox"
        checked={checked}
        onChange={handleChange}
        />
    </div>
  )
}

export default NoteItem;
