import './LoginForm.css';

import { useState } from 'react';
import { useNavigate, Link } from "react-router-dom";


function LoginForm({title, handleSubmit, link}) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleName = (event) => {
    event.preventDefault();
    setUsername(event.target.value);
  }

  const handlePassword = (event) => {
    event.preventDefault();
    setPassword(event.target.value);
  }

  const submitForm = (event) => {
    event.preventDefault();
    handleSubmit(username, password)
  }
  
  return (
    <form className='form' onSubmit={(event) => submitForm(event)}>
      <h2>{title}</h2>
      <label>
        Username:
        <input
          type="text"
          name="name"
          autoComplete="off"
          value={username}
          onChange={(event) => handleName(event)}
        />
      </label>
      <label>
        Password:
        <input
          type="password"
          name="name"
          autoComplete="off"
          onChange={(event) => handlePassword(event)}
        />
      </label>
      <input className='btn__add' type="submit" value={title} />
      <Link to={`/${link}`}>Follow to {link}</Link>
    </form>
  )
}

export default LoginForm;
