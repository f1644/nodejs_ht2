import './App.css';
import { Routes, Route, Navigate } from 'react-router-dom';

import NoteList from './components/NoteList/NoteList';
import Header from './components/Header/Header';
import AddNotes from './components/AddNote/AddNote';
import NotesPage from './pages/NotesPage/NotesPage';
import AddNotesPage from './pages/AddNotePage/AddNotePage';
import LoginPage from './pages/LoginPage/LoginPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';

function App() {
  const token = localStorage.getItem('token');
  return (
    <div className="App">
      <div className="wrapper">
        <Header 
          token={token}
        />
        <Routes>
          <Route path='/' element={token ? (
            <Navigate to='/notes' replace />
          ) : (
            <Navigate to='/login' replace />
          )}/>
          <Route path='/notes' element={<NotesPage />}></Route>
          <Route path='/add' element={<AddNotesPage />}></Route>
          <Route path='/register' element={<RegisterPage />}></Route>
          <Route path='/login' element={<LoginPage />}></Route>
        </Routes>
      </div>
      
    </div>
  );
}

export default App;
