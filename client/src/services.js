export const MAIN_LINK = 'http://localhost:8080/api';

async function getResponse(url, obj = null) {
  let result;

  if (obj !== null) {
    result = await fetch(url, obj);
  } else {
    result = await fetch(url);
  }

  if (!result.ok) {
    throw new Error(`Could not fetch ${url}, status: ${result.status}`);
  }

  const fromJson = await result.json();
  return fromJson;
}

export async function getAllUserNotes(token) {
  const recHeaders = new Headers();
  recHeaders.set('authorization', `Bearer ${token}`);
  const res = await getResponse(`${MAIN_LINK}/notes?offset=1&limit=2`, {
    method: 'GET',
    headers: recHeaders,
  });
  return res;
}

export async function changeCompletedNote(id, token) {
  const recHeaders = new Headers();
  recHeaders.set('authorization', `Bearer ${token}`);
  const res = await getResponse(`${MAIN_LINK}/notes/${id}`, {
    method: 'PATCH',
    headers: recHeaders,
  });
  return res;
}

export async function createUser(params) {
  const res = await getResponse(`${MAIN_LINK}/auth/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(params),
  });
  return res;
}

export async function loginUser(params) {
  const res = await getResponse(`${MAIN_LINK}/auth/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(params),
  });
  return res;
}

export async function addNotes(params, token) {
  const recHeaders = new Headers();
  recHeaders.set('authorization', `Bearer ${token}`);
  recHeaders.set('Content-Type', 'application/json');
  const res = await getResponse(`${MAIN_LINK}/notes`, {
    method: 'POST',
    headers: recHeaders,
    body: JSON.stringify(params),
  });
  return res;
}
