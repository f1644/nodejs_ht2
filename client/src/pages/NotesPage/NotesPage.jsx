import { useNavigate, Link } from "react-router-dom";

import NoteList from "../../components/NoteList/NoteList";
import AddNotes from "../../components/AddNote/AddNote";

function NotesPage() {
  const token = localStorage.getItem('token');
  let navigate = useNavigate();
  if (!token) {
    navigate("/login", { replace: true });
  }
  return (
    <div className='notes__page'>
      <NoteList />
      <AddNotes />
    </div>
  )
}

export default NotesPage;
