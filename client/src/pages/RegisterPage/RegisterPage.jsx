import { useNavigate, Link } from "react-router-dom";

import { createUser } from '../../services';

import LoginForm from "../../components/LoginForm/LoginForm";

function RegisterPage() {
  let navigate = useNavigate();
  function register (username, password) {
    try {
      createUser({
        username,
        password
        }).then(() => {
          navigate("/login", { replace: true });
        });
    } catch(e) {
      console.log(e)
    }
  }
  
  return (
    <div className='form'>
      <LoginForm
        title='Register'
        handleSubmit={register}
        link='login'
      />
    </div>
  )
}

export default RegisterPage;
