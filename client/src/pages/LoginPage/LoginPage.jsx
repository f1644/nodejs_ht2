import { useNavigate } from "react-router-dom";

import { loginUser } from '../../services';
import LoginForm from "../../components/LoginForm/LoginForm";

function LoginPage() {
  let navigate = useNavigate();
  function login (username, password) {
    try {
      loginUser({
        username,
        password
        }).then((data) => {
          navigate("/notes", { replace: true });
          localStorage.setItem('token', data.jwt_token);
          window.location.reload();
        });
    } catch(e) {
      console.log(e)
    }
  }
  
  return (
    <div className='form'>
      <LoginForm
        title='Login'
        handleSubmit={login}
        link='register'
      />
    </div>
  )
}

export default LoginPage;
