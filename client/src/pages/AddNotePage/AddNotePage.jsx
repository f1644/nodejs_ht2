import { useState } from 'react';
import { useNavigate, Link } from "react-router-dom";
import { addNotes } from '../../services';
import './AddNotePage.css';

function AddNotesPage() {
  const [text, setText] = useState('');
  const token = localStorage.getItem('token');
  let navigate = useNavigate();

  const handleText = (event) => {
    event.preventDefault();
    setText(event.target.value);
  }

  const addNote = (event) => {
    event.preventDefault();

    try {
      addNotes({text}, token).then(() => {
          navigate("/notes", { replace: true });
        });
    } catch(e) {
      console.log(e)
    }
  }

  return (
    <div className='add-notes__page'>
      <Link to={`/notes`}>Follow to notes</Link>
      <form className='add__form' onSubmit={(event) => addNote(event)}>
      <textarea
        type="text"
        name="name"
        rows="10"
        autoComplete="off"
        value={text}
        onChange={(event) => handleText(event)}
      />
      <input className='btn__add' type="submit" value={'Add'} />
      
    </form>
      
    </div>
  )
}

export default AddNotesPage;
